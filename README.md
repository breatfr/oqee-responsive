# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/breatfr) <a href="https://www.paypal.me/breat"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>
Le site web [OQEE](https://oqee.tv/) est mieux adapté aux écrans larges.
## Prévisualisation
![Preview](https://gitlab.com/breatfr/oqee/-/raw/main/docs/preview.jpg)

## Liste des personnalisations disponible
- taille du texte personnalisée

## Comment l'utiliser en quelques étapes
1. Installer l'extension de navigateur Stylus
    - Lien pour les navigateurs basés sur Chromium : https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne
        - Brave
        - Chromium
        - Google Chrome
        - Iridium Browser
        - Microsoft Edge
        - Opera
        - Opera GX
        - SRWare Iron
        - Ungoogled Chromium
        - Vivaldi
        - Yandex Browser
        - many more
    - Lien pour les navigateurs basés sur Firefox : https://addons.mozilla.org/firefox/addon/styl-us/
        - Mozilla Firefox
        - Mullvad Browser
        - Tor Browser
        - Waterfox
        - many more

2. Allez sur le site [UserStyles.world](https://userstyles.world/style/16588) et cliquez sur `Install` en dessous de l'image ou ouvre la [version GitLab](https://gitlab.com/breatfr/oqee/-/raw/main/css/oqee-responsive.user.css).

3. Pour mettre à jour le thème, ouvrez la fenêtre `Gestion des styles` et cliquez sur `Vérifier les mises à jour` et suivez les instructions ou attendez simplement 24h pour la mise à jour automatique.

4. Profitez :)
# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/breatfr) <a href="https://www.paypal.me/breat"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>